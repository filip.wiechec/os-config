#!/usr/bin/env zsh
# vim: set sw=2:

ansible_plugins=(
"https://github.com/kewlfft/ansible-aur.git aur"
)
ansible_plugins_dir="${HOME}/.ansible/plugins/modules"

declare -a ansible_playbook_args

red="$(tput setaf 1)"
green="$(tput setaf 2)"
reset="$(tput sgr0)"

function usage () {
  echo -e "USAGE:\t${0} [-h|--help] [-f/--fonts] [-p/--packages] [-v/--virtualization] [-d/--hidpi] [ANSIBLE_PLAYBOOK_ARGUMENTS]"
  exit 0
}

function handle_args () {
  for arg in ${@}; do
    case ${arg} in
      -h|--help)
        usage
        ;;
      -f|--fonts)
        ansible_playbook_args+=("-e install_fonts=true")
        ;;
      -p|--packages)
        ansible_playbook_args+=("-e install_software=true")
        ;;
      -v|--virtualization)
        ansible_playbook_args+=("-e virtualization=true")
        ;;
      -d|--hidpi)
        ansible_playbook_args+=("-e hidpi=true")
        ;;
      *)
        ansible_playbook_args+=("${arg}") ;;
    esac
  done
}

function info_msg () {
  local msg="${green}[$(date +'%T')] INFO: "
  echo -e "${msg}${1}${reset}"
}

function error_msg () {
  local msg="${red}[$(date +'%T')] [${BASH_LINENO[0]}] ERROR: "
  echo -e "${msg}${1}${reset}" > /dev/stderr
  return "${2:-0}"
}

function install_ansible_plugins () {
  info_msg "Installing required Ansible plugins..."
  [[ -d "${ansible_plugins_dir}" ]] || mkidr "${ansible_plugins_dir}"
  for plugin_url_dir in ${ansible_plugins[@]}; do
    local plugin_url="${plugin_url_dir% *}"
    local plugin_dir="${plugin_url_dir#* }"
    local destination="${ansible_plugins_dir}/${plugin_dir}"
    if ! [[ -d "${destination}" ]]; then
      if git clone "${plugin_url}" "${destination}" &> /dev/null; then
        info_msg "Installed ${plugin_dir} into ${destination}"
      else
        error_msg "Plugin ${plug} installation failed." 1
      fi
    else
      info_msg "${destination} already exists. Skipping..."
    fi
  done
}

function run_playbook () {
  pushd "${0:A:h}/ansible/"
  ansible-playbook -i inventory os-config.yaml --ask-become-pass ${@}
  popd
}

function main () {
  handle_args ${@}
  install_ansible_plugins
  run_playbook ${ansible_playbook_args}
}

main ${@}
